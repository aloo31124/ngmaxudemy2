import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// ch11
import { Ch11HomeComponent } from './ch11/component/ch11-home/ch11-home.component'; 
import { ServerComponent } from './ch11/component/server/server.component';
import { ServerListComponent } from './ch11/component/server-list/server-list.component';
import { ServerEditComponent } from './ch11/component/server-edit/server-edit.component';
import { UserComponent } from './ch11/component/user/user.component';
import { UserlistComponent } from './ch11/component/userlist/userlist.component';
import { RouterModule, Routes } from '@angular/router';
import { Ch7HomeComponent } from './ch7-dir/components/ch7-home/ch7-home.component';
import { BasiceHeightlightDirective } from './ch7-dir/directive/basice-heightlight.directive';
import { BetterHeightlightDirective } from './ch7-dir/directive/better-heightlight.directive';
import { UnlessDirective } from './ch7-dir/directive/unless.directive';
import { Ch5HomeComponent } from './ch5-data-bind/components/ch5-home/ch5-home.component';
import { CockpitComponent } from './ch5-data-bind/components/cockpit/cockpit.component';
import { ServerElementComponent } from './ch5-data-bind/components/server-element/server-element.component';
import { FormsModule } from '@angular/forms';
import { Ch9HomeComponent } from './ch9-di-service/components/ch9-home/ch9-home.component';
import { AccountComponent } from './ch9-di-service/components/account/account.component';
import { AccountAddComponent } from './ch9-di-service/components/account-add/account-add.component';

// app.module.ts error TS2724: '"@angular/router"' has no exported member named 'Routers'. Did you mean 'Router'?
const routers: Routes = [
  // ch11 router
  {path: 'ch11-home', component: Ch11HomeComponent},
  {path: 'server-list', component: ServerListComponent},
  {path: 'server-list/:id/edit', component: ServerEditComponent},
  {path: 'user-list', component: UserlistComponent},
  {path: 'user-list/:id/:name', component: UserComponent}, // ch11-136,137
];

@NgModule({
  declarations: [
    AppComponent,

    //ch9
    Ch9HomeComponent,
    AccountComponent,
    AccountAddComponent,

    //ch11
    Ch11HomeComponent,
    ServerComponent,
    ServerListComponent,
    ServerEditComponent,
    UserComponent,
    UserlistComponent,
    Ch7HomeComponent,
    BasiceHeightlightDirective,
    BetterHeightlightDirective,
    UnlessDirective,
    Ch5HomeComponent,
    CockpitComponent,
    ServerElementComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routers),
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
