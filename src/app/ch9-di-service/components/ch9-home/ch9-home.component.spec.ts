import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Ch9HomeComponent } from './ch9-home.component';

describe('Ch9HomeComponent', () => {
  let component: Ch9HomeComponent;
  let fixture: ComponentFixture<Ch9HomeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [Ch9HomeComponent]
    });
    fixture = TestBed.createComponent(Ch9HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
