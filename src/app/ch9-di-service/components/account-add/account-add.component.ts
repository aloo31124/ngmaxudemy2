import { Component } from '@angular/core';
import { LoggingService } from '../../services/logging.service';

@Component({
  selector: 'app-account-add',
  templateUrl: './account-add.component.html',
  styleUrls: ['./account-add.component.css'],
  // providers: [LoggingService] // ch9-110. 需補上
})
export class AccountAddComponent {

  constructor(private logginService: LoggingService) {}

  printLogError() {
    //ch9-109. 使用 服務 錯誤範例
    const loggingService1 = new LoggingService();
    loggingService1.logStatusChange("error ex 錯誤範例");
  }

  printLog() {
    //ch9-110. 使用 服務 正確範例
    this.logginService.logStatusChange("正確印出範例");
  }

}
