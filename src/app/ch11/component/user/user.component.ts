import { Component, OnDestroy, OnInit } from '@angular/core';
import { User } from '../../model/user';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, OnDestroy {
  user = new User();
  // ch11-139. 路由訂閱銷毀
  paramsSubscription!: Subscription;
  
  constructor(
    private activedRouter: ActivatedRoute
  ) {}

  ngOnInit(): void {
    // ch11- 136,137 
    this.user.id = +this.activedRouter.snapshot.params['id'];
    this.user.name = this.activedRouter.snapshot.params['name'];
    console.log(this.user);

    // ch11- 138. 使用 Activated route paramter, 訂閱 => 可解決元件內外層不同步
    this.paramsSubscription = this.activedRouter.params
      .subscribe((paramer) => {
        this.user.id = +this.activedRouter.snapshot.params['id'];
        this.user.name = this.activedRouter.snapshot.params['name'];
        console.log(this.user);
      });
  }


  ngOnDestroy(): void {
    // ch11-139. 路由訂閱取消
    this.paramsSubscription.unsubscribe();
  }
  
}
