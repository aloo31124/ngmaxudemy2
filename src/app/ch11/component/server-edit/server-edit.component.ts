import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-server-edit',
  templateUrl: './server-edit.component.html',
  styleUrls: ['./server-edit.component.css']
})
export class ServerEditComponent implements OnInit {

  constructor(
    private activeRoute: ActivatedRoute
  ){}

  ngOnInit(): void {
    //ch11-140,141 取得參數
    console.log(this.activeRoute.snapshot.params);
    console.log(this.activeRoute.snapshot.fragment);
    this.activeRoute.params.subscribe();
    this.activeRoute.fragment.subscribe();
    
  }

}
