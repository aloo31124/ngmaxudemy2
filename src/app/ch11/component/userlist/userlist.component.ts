import { Component } from '@angular/core';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent {

  userList = [
    {
      id: 1,
      name: "loulou",
    },
    {
      id: 2,
      name: "lucy",
    },
    {
      id: 3,
      name: "jack",
    },
  ]

}
