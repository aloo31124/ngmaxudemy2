import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-server-list',
  templateUrl: './server-list.component.html',
  styleUrls: ['./server-list.component.css']
})
export class ServerListComponent {

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute
  ) {}

  public routeReload1 () {
    this.router.navigate(["./server-list"]);
  }

  public routeReload2 () {
    this.router.navigate(["./server-list"],{relativeTo: this.activeRoute});
  }

  routeServerEdit() {
    this.router.navigate(["/server-list", 878, "edit"], {queryParams: {isAllowEdit: 1},fragment: "loading2"});
  }

}
