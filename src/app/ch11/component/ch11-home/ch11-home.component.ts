import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ch11-home',
  templateUrl: './ch11-home.component.html',
  styleUrls: ['./ch11-home.component.css']
})
export class Ch11HomeComponent {
  /*
   * ch11-139. 路由訂閱銷毀
   * ch11-140,141 取得參數 兩中方式 + 編輯sever, server-list/:id/edit?p=a
   */

  constructor(private router: Router) {}

  public routerToServerList() {
    this.router.navigate(['./server-list']);
  }

}
