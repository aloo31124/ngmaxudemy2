import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Ch11HomeComponent } from './ch11-home.component';

describe('Ch11HomeComponent', () => {
  let component: Ch11HomeComponent;
  let fixture: ComponentFixture<Ch11HomeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [Ch11HomeComponent]
    });
    fixture = TestBed.createComponent(Ch11HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
