import { Component, Input } from '@angular/core';
import { Server } from '../../models/server';

@Component({
  selector: 'app-server-element',
  templateUrl: './server-element.component.html',
  styleUrls: ['./server-element.component.css']
})
export class ServerElementComponent {

  @Input('showServerElement') element!: Server;

}
