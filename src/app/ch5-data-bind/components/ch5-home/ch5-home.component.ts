import { Component, ContentChild, ViewEncapsulation } from '@angular/core';
import { Server } from '../../models/server';

// ch5-66~73 新增, 顯示 藍紅 server
// ch5 74,75, 封裝 encapsulation
// ch5 76 範本 變數 reference template #
// ch5 77,78 @ViewChild 取得 ＃範本變數 之 dom

@Component({
  selector: 'app-ch5-home',
  templateUrl: './ch5-home.component.html',
  styleUrls: ['./ch5-home.component.css'],
  encapsulation: ViewEncapsulation.Emulated // ch 74, 75: none, showDom, emulated
})
export class Ch5HomeComponent {

  serverList: Server[] = [
    {name:"server1", type:"red", content: "content red yo!"},
    {name:"server2", type:"blue", content: "blue blue!"}
  ];

  addBlue(server: Server) {
    this.serverList.push(server);
  }

  addRed (server: Server) {
    this.serverList.push(server);
  }


}
