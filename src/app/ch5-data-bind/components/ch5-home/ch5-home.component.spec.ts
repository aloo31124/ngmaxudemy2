import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Ch5HomeComponent } from './ch5-home.component';

describe('Ch5HomeComponent', () => {
  let component: Ch5HomeComponent;
  let fixture: ComponentFixture<Ch5HomeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [Ch5HomeComponent]
    });
    fixture = TestBed.createComponent(Ch5HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
