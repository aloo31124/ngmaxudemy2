import { Component, ElementRef, EventEmitter, Output, ViewChild } from '@angular/core';
import { Server } from '../../models/server';

@Component({
  selector: 'app-cockpit',
  templateUrl: './cockpit.component.html',
  styleUrls: ['./cockpit.component.css']
})
export class CockpitComponent {

  @Output() addRed = new EventEmitter<Server>();
  @Output() addBlue = new EventEmitter<Server>();

  @ViewChild('inputContent')inputContent!: ElementRef;

  newServerName = "";
  newServerContent = "";

  createRed() {
    this.addRed.emit({
      name: this.newServerName,
      content: this.newServerContent,
      type: "red"
    });
  }
  
  createBlue() {
    this.addRed.emit({
      name: this.newServerName,
      content: this.newServerContent,
      type: "blue"
    });
  }

  getTemplateRef(inputValue: any) {
    console.log(inputValue);
    console.log(inputValue.value);
  }

  //ch5-78. 取得該元素 elementRef
  getElement() {
    console.log(this.inputContent.nativeElement);
    //取得輸入值
    console.log(this.inputContent.nativeElement.value);
    //取得高？
    console.log(this.inputContent.nativeElement.offsetHeight);
  }

}
