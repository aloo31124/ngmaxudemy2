import { Directive, ElementRef, HostBinding, HostListener, Input, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appBetterHeightlight]'
})
export class BetterHeightlightDirective implements OnInit {

  // 背景初始色
  @Input() defaultBackgroundColor = "green";
  // 背景亮色
  @Input() heightlghtBackgroundColor!: string;
  // 綁定 該元素背景色 物件
  @HostBinding('style.backgroundColor') backgorundColor!: string;

  constructor(
    private renderer2: Renderer2,
    private elementRef: ElementRef
  ) { }

  ngOnInit(): void {
    //ch7-97. 使用 Renderer2 + ElementRef 改變背景顏色
    this.renderer2.setStyle(this.elementRef.nativeElement, 'color', 'red');
    this.backgorundColor = this.defaultBackgroundColor;
  }

  //滑鼠移入, 文字變藍色
  @HostListener('mouseenter') mouseEnter(event: Event) {
    this.renderer2.setStyle(this.elementRef.nativeElement, 'color', 'blue');
    this.backgorundColor = this.heightlghtBackgroundColor;
  } 
  
  //滑鼠移除, 文字變紅色
  @HostListener('mouseleave') mouseLeave(){
    this.renderer2.setStyle(this.elementRef.nativeElement, 'color', 'red');
    this.backgorundColor = this.defaultBackgroundColor;
  }

}
