import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appUnless]'
})
export class UnlessDirective {

  @Input() set checkUnless(isUnless: boolean) {
    if(isUnless) {
      this.viewContainerRef.clear();
    } else {
      this.viewContainerRef.createEmbeddedView(this.templateRef);
    } 
  }

  constructor(
    private viewContainerRef: ViewContainerRef, // ?
    private templateRef: TemplateRef<any>, // ?
  ) { }

}
