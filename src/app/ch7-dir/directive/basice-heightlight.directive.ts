import { Directive, ElementRef, OnInit } from '@angular/core';

@Directive({
  selector: '[loulouBasiceHeightlight]' // 可自定義
})
export class BasiceHeightlightDirective implements OnInit {

  constructor(private elementRef: ElementRef) { }

  ngOnInit(): void {
    this.elementRef.nativeElement.style.color = "green";
  }

}
