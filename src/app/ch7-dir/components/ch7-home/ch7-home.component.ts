import { Component } from '@angular/core';

  /*
    ch94. 建立 奇數, 偶數 陣列， 並用按鈕切換
    ch95. ngClass/ngStyle 使用 js 物件{} 判斷顏色
    ch96. * 手動新增 (attritube) directive 注入 該 html 元素 => 直接使用 ElementRef
    ch97. * 指令新增 (attritube) directive 注入 該 html 元素 => 使用 Render2
    ch98. 小文件
    ch99. 使用 hostingListen 監聽事件
    ch100. 使用 hostingBindeing 綁定當前 元素之 屬性property 
    ch101. @input
    ch102. [ngIf]  ?
    ch103. 
     
 */
@Component({
  selector: 'app-ch7-home',
  templateUrl: './ch7-home.component.html',
  styleUrls: ['./ch7-home.component.css']
})
export class Ch7HomeComponent {
  
  oddNumberList = [1, 3, 5, 7];
  eventNumberList = [2, 4, 6, 8];
  isOdd = true;

}
