import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Ch7HomeComponent } from './ch7-home.component';

describe('Ch7HomeComponent', () => {
  let component: Ch7HomeComponent;
  let fixture: ComponentFixture<Ch7HomeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [Ch7HomeComponent]
    });
    fixture = TestBed.createComponent(Ch7HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
